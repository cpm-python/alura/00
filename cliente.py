class Cliente:
    def __init__(self, nome):
        self.__nome = nome

    @property
    def nome(self):
        print("Chamendo: get {}".format(self.__nome))
        return self.__nome.title()

    @nome.setter
    def nome(self, nome):
        print("Chamendo: set {}".format(self.__nome))
        self.__nome = nome